/*************************************************************************************************
  Required Notice: Copyright (C) EPPlus Software AB. 
  This software is licensed under PolyForm Noncommercial License 1.0.0 
  and may only be used for noncommercial purposes 
  https://polyformproject.org/licenses/noncommercial/1.0.0/

  A commercial license to use this software can be purchased at https://epplussoftware.com
 *************************************************************************************************
  Date               Author                       Change
 *************************************************************************************************
  01/27/2020         EPPlus Software AB       Initial release EPPlus 5
 *************************************************************************************************/
using OfficeOpenXml.FormulaParsing.LexicalAnalysis;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace OfficeOpenXml.Utils
{
    /// <summary>
    /// A utility to work with Excel addresses
    /// </summary>
    public static class AddressUtility
    {
        /// <summary>
        /// Parse an entire column selection, e.g A:A
        /// </summary>
        /// <param name="address">The entire address</param>
        /// <returns></returns>
        public static string ParseEntireColumnSelections(string address)
        {
            string parsedAddress = address;
            var matches = Regex.Matches(address, "[A-Z]+:[A-Z]+");
            foreach (Match match in matches)
            {
                AddRowNumbersToEntireColumnRange(ref parsedAddress, match.Value);
            }
            return parsedAddress;
        }
        /// <summary>
        /// Add row number to entire column range
        /// </summary>
        /// <param name="address">The address</param>
        /// <param name="range">The full column range</param>
        private static void AddRowNumbersToEntireColumnRange(ref string address, string range)
        {
            var parsedRange = string.Format("{0}{1}", range, ExcelPackage.MaxRows);
            var splitArr = parsedRange.Split(new char[] { ':' });
            address = address.Replace(range, string.Format("{0}1:{1}", splitArr[0], splitArr[1]));
        }

        internal static string ShiftAddressRowsInFormula(ExcelRangeBase range, string formula, int currentRow, int rows)
        {
            if (string.IsNullOrEmpty(formula)) return formula;
            var affectedRange = new ExcelAddressBase(range.ExternalReferenceIndex, range.WorkSheetName, range._fromRow, range._fromCol, range._toRow, range._toCol);
            var tokens = SourceCodeTokenizer.Default.Tokenize(formula, affectedRange.WorkSheetName);
            if (!tokens.Any(x => x.TokenTypeIsAddress)) return formula;
            var resultTokens = new List<Token>();
            string extRef = string.Empty, ws = string.Empty;
            foreach (var token in tokens)
            {
                if(token.TokenTypeIsAddressToken)
                {
                    if (token.TokenType == TokenType.WorksheetNameContent)
                    {
                        ws=token.Value;
                    }
                    else if(token.TokenType==TokenType.ExternalReference)
                    {
                        extRef = token.Value;
                    }
                }
                else
                {
                    extRef = ws = string.Empty;
                }

                if (!token.TokenTypeIsAddress)
                {
                    resultTokens.Add(token);
                }
                else
                {
                    if (token.TokenTypeIsSet(TokenType.CellAddress) || token.TokenTypeIsSet(TokenType.ExcelAddress))
                    {
                        var addresses = new List<ExcelCellAddress>();
                        var adr = new ExcelAddressBase(token.Value);
                        // if the formula is a table formula (relative) keep it as it is
                        if (adr.Table == null && (adr.Collide(affectedRange) != ExcelAddressBase.eAddressCollition.No) && 
                                ((extRef == string.Empty && ws == string.Empty) || 
                                (ws.Equals(range.WorkSheetName, StringComparison.InvariantCultureIgnoreCase) && extRef == string.Empty) ||
                                 int.TryParse(extRef, out int ier) && ier==range.ExternalReferenceIndex))
                        {
                            var newAdr = adr.AddRow(currentRow, rows, true);
                            var newToken = new Token(newAdr.FullAddress, token.TokenType);
                            resultTokens.Add(newToken);
                        }
                        else
                        {
                            resultTokens.Add(token);
                        }
                    }
                    else if (token.Value.StartsWith("$") == false && token.TokenTypeIsSet(TokenType.FullRowAddress) && int.TryParse(token.Value, out int r))
                    {
                        r += rows;
                        if (r >= affectedRange._fromRow && r <= affectedRange._toRow)
                        {
                            var newToken = new Token(r.ToString(CultureInfo.InvariantCulture), token.TokenType);
                            resultTokens.Add(newToken);
                        }
                        else 
                        { 
                            resultTokens.Add(token);
                        }
                    }
                    else
                    {
                        resultTokens.Add(token);
                    }
                }
            }
            var result = new StringBuilder();
            foreach (var token in resultTokens)
            {
                result.Append(token.Value);
            }
            return result.ToString();
        }

        internal static string ShiftAddressColumnsInFormula(ExcelRangeBase range, string formula, int currentColumn, int columns)
        {
            if (string.IsNullOrEmpty(formula)) return formula;
            var affectedRange = new ExcelAddressBase(range.ExternalReferenceIndex <= 0 ? -1 : range.ExternalReferenceIndex, range.WorkSheetName, range._fromRow, range._fromCol, range._toRow, range._toCol);
            var tokens = SourceCodeTokenizer.Default.Tokenize(formula, affectedRange.WorkSheetName);
            if (tokens.Any(x => x.TokenTypeIsAddress)==false) return formula;
            var resultTokens = new List<Token>();
            string extRef = string.Empty, ws = string.Empty;
            foreach (var token in tokens)
            {
                if (token.TokenTypeIsAddressToken)
                {
                    if (token.TokenType == TokenType.WorksheetNameContent)
                    {
                        ws = token.Value;
                    }
                    else if (token.TokenType == TokenType.ExternalReference)
                    {
                        extRef = token.Value;
                    }
                }
                if (token.TokenTypeIsSet(TokenType.ExcelAddress) || token.TokenTypeIsSet(TokenType.CellAddress))
                {
                    var addresses = new List<ExcelCellAddress>();
                    var adr = new ExcelAddressBase(token.Value);
                    //if the formula is a table formula (relative) keep it as it is.
                    if (adr.Table == null && adr.Collide(affectedRange) != ExcelAddressBase.eAddressCollition.No &&
                                ((extRef == string.Empty && ws == string.Empty) ||
                                (ws.Equals(range.WorkSheetName, StringComparison.InvariantCultureIgnoreCase) && extRef == string.Empty) ||
                                 int.TryParse(extRef, out int ier) && ier == range.ExternalReferenceIndex))
                    {
                        var newAdr = adr.AddColumn(currentColumn, columns, true);
                        var newToken = new Token(newAdr.FullAddress, TokenType.ExcelAddress);
                        resultTokens.Add(newToken);
                    }
                    else
                    {
                        resultTokens.Add(token);
                    }
                }
                else if (token.Value.StartsWith("$") == false && token.TokenTypeIsSet(TokenType.FullColumnAddress) && ExcelCellBase.IsColumnLetter(token.Value))
                {                    
                    var c = ExcelCellBase.GetColumn(token.Value);
                    if (c >= affectedRange._fromCol && c <= affectedRange._toCol)
                    {
                        c += columns;
                        var newToken = new Token(ExcelCellBase.GetColumnLetter(c), token.TokenType);
                        resultTokens.Add(newToken);
                    }
                    else
                    {
                        resultTokens.Add(token);
                    }
                }
                else
                {
                    resultTokens.Add(token);
                }
            }
            var result = new StringBuilder();
            foreach (var token in resultTokens)
            {
                result.Append(token.Value);
            }
            return result.ToString();
        }
    }
}
